/* usmb - mount SMB shares via FUSE and Samba
 * Copyright (C) 2006-2009 Geoff Johnstone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef USMB_DIR_H
  #define USMB_DIR_H

  #include <sys/types.h>
  #include <fuse.h>

  int usmb_mkdir (const char *dirname, mode_t mode);
  int usmb_rmdir (const char *dirname);
  int usmb_opendir (const char *dirname, struct fuse_file_info *fi);
  int usmb_readdir (const char *path, void *h, fuse_fill_dir_t filler,
                    off_t offset, struct fuse_file_info *fi);
  int usmb_releasedir (const char *path, struct fuse_file_info *fi);
  int usmb_setxattr (const char *path, const char *name, const char *value,
                     size_t size, int flags);
  int usmb_getxattr (const char *path, const char *name, char *value,
                     size_t size);
  int usmb_listxattr (const char *path, char *list, size_t size);
  int usmb_removexattr (const char *path, const char *name);

#endif
