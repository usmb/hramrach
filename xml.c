/* usmb - mount SMB shares via FUSE and Samba
 * Copyright (C) 2006-2010 Geoff Johnstone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"
#include <libxml/xmlreader.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>
#include <assert.h>
#include <stdbool.h>
#include <string.h>
#include "xml.h"
#include "utils.h"


bool xml_validate_relaxng (xmlDocPtr doc, const char *schema)
{
  xmlRelaxNGParserCtxtPtr rngpcptr = NULL;
  xmlRelaxNGPtr rngptr = NULL;
  xmlRelaxNGValidCtxtPtr rngvptr = NULL;
  bool ret = false;

  assert (NULL != doc);

  do
  {
    rngpcptr = xmlRelaxNGNewMemParserCtxt (schema, strlen (schema));
    if (NULL == rngpcptr)
      break;

    rngptr = xmlRelaxNGParse (rngpcptr);
    if (NULL == rngptr)
      break;

    rngvptr = xmlRelaxNGNewValidCtxt (rngptr);
    if (NULL == rngvptr)
      break;

    ret = (0 == xmlRelaxNGValidateDoc (rngvptr, doc));
  } while (false /*CONSTCOND*/);

  if (NULL != rngvptr)
    xmlRelaxNGFreeValidCtxt (rngvptr);

  if (NULL != rngptr)
    xmlRelaxNGFree (rngptr);

  if (NULL != rngpcptr)
    xmlRelaxNGFreeParserCtxt (rngpcptr);

  return ret;
}


bool xml_xpath_attr_value (xmlXPathContextPtr ctx,
                           char *xpath,
                           const char *attr,
                           char **out)
{
  xmlXPathObjectPtr obj;
  xmlChar *tmp;

  assert (NULL != ctx);
  assert (NULL != xpath);
  assert (NULL != out);

  *out = NULL;

  obj = xmlXPathEval (BAD_CAST xpath, ctx);
  if (NULL == obj)
  {
    DEBUG (fputs ("XPath evaluation error\n", stderr));
    return false;
  }

  do
  {
    if (XPATH_NODESET != obj->type)
    {
      DEBUG (fputs ("XPath evaluation didn't return a nodeset\n", stderr));
      break;
    }

    if (NULL == obj->nodesetval)
    {
      DEBUG (fputs ("nodesetval is NULL\n", stderr));
      break;
    }

    if (1 != obj->nodesetval->nodeNr)
    {
      DEBUG (fprintf (stderr, "Nodeset has %d elements\n",
                          obj->nodesetval->nodeNr));
      break;
    }

    tmp = xmlGetProp (obj->nodesetval->nodeTab[0], BAD_CAST attr);
    if (NULL == tmp)
      break;

    *out = xstrdup ((char *)tmp);
    if (NULL == *out)
      break;

    xmlXPathFreeObject (obj);
    return true;
    /*NOTREACHED*/
  } while (false /*CONSTCOND*/);

  *out = NULL;
  xmlXPathFreeObject (obj);
  return false;
}


bool xml_xpath_text (xmlXPathContextPtr ctx, char *xpath, char **out)
{
  xmlXPathObjectPtr obj;

  assert (NULL != ctx);
  assert (NULL != xpath);
  assert (NULL != out);

  *out = NULL;

  DEBUG (fprintf (stderr, "xml_xpath_text (%s)\n", xpath));

  obj = xmlXPathEval (BAD_CAST xpath, ctx);
  if (NULL == obj)
  {
    DEBUG (fputs ("XPath evaluation error\n", stderr));
    return false;
  }

  do
  {
    if (XPATH_NODESET != obj->type)
    {
      DEBUG (fputs ("XPath evaluation didn't return a nodeset\n", stderr));
      break;
    }

    if (NULL == obj->nodesetval)
    {
      DEBUG (fputs ("nodesetval is NULL\n", stderr));
      break;
    }

    if (1 != obj->nodesetval->nodeNr)
    {
      DEBUG (fprintf (stderr, "Nodeset has %d elements\n",
                          obj->nodesetval->nodeNr));
      break;
    }

    if (NULL == obj->nodesetval->nodeTab[0]->content)
    {
      DEBUG (fputs ("Node has no text\n", stderr));
      break;
    }

    *out = xstrdup ((char *)obj->nodesetval->nodeTab[0]->content);
    if (NULL == *out)
      break;

    xmlXPathFreeObject (obj);
    return true;
    /*NOTREACHED*/
  } while (false /*CONSTCOND*/);

  *out = NULL;
  xmlXPathFreeObject (obj);
  return false;
}

