/* usmb - mount SMB shares via FUSE and Samba
 * Copyright (C) 2006-2009 Geoff Johnstone
 *
 * Portions of this file are taken from Samba 3.2's libsmbclient.h:
 *  Copyright (C) Andrew Tridgell 1998
 *  Copyright (C) Richard Sharpe 2000
 *  Copyright (C) John Terpsra 2000
 *  Copyright (C) Tom Jansen (Ninja ISD) 2002
 *  Copyright (C) Derrell Lipman 2003-2008
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SAMBA_30_COMPAT_H
  #define SAMBA_30_COMPAT_H

  #include "config.h"
  #include <libsmbclient.h>
  #include <sys/statvfs.h>


  int usmb_statfs (const char *path UNUSED, struct statvfs *vfs UNUSED);
  int compat_truncate (const char *path, SMBCFILE *file, off_t size);


  #ifndef HAVE_SAMBA32

    typedef int (*smbc_chmod_fn) (SMBCCTX *c, const char *fname, mode_t mode);

    static inline smbc_chmod_fn smbc_getFunctionChmod (SMBCCTX *c)
    {
      return c->chmod;
    }


    typedef int (*smbc_close_fn) (SMBCCTX *c, SMBCFILE *file);

    static inline smbc_close_fn smbc_getFunctionClose (SMBCCTX *c)
    {
      return c->close_fn;
    }


    typedef int (*smbc_closedir_fn) (SMBCCTX *c, SMBCFILE *dir);

    static inline smbc_closedir_fn smbc_getFunctionClosedir (SMBCCTX *c)
    {
      return c->closedir;
    }


    typedef SMBCFILE * (*smbc_creat_fn) (SMBCCTX *c,
                                         const char *path,
                                         mode_t mode);

    static inline smbc_creat_fn smbc_getFunctionCreat (SMBCCTX *c)
    {
      return c->creat;
    }


    typedef int (*smbc_fstat_fn) (SMBCCTX *c, SMBCFILE *file, struct stat *st);

    static inline smbc_fstat_fn smbc_getFunctionFstat (SMBCCTX *c)
    {
      return c->fstat;
    }


    typedef int (*smbc_getxattr_fn) (SMBCCTX *context,
                                     const char *fname,
                                     const char *name,
                                     const void *value,
                                     size_t size);

    static inline smbc_getxattr_fn smbc_getFunctionGetxattr (SMBCCTX *c)
    {
      return c->getxattr;
    }


    typedef int (*smbc_listxattr_fn) (SMBCCTX *context,
                                      const char *fname,
                                      char *list,
                                      size_t size);

    static inline smbc_listxattr_fn smbc_getFunctionListxattr (SMBCCTX *c)
    {
      return c->listxattr;
    }


    typedef off_t (*smbc_lseek_fn) (SMBCCTX *c,
                                    SMBCFILE *file,
                                    off_t offset,
                                    int whence);

    static inline smbc_lseek_fn smbc_getFunctionLseek (SMBCCTX *c)
    {
      return c->lseek;
    }


    typedef int (*smbc_mkdir_fn) (SMBCCTX *c, const char *fname, mode_t mode);

    static inline smbc_mkdir_fn smbc_getFunctionMkdir (SMBCCTX *c)
    {
      return c->mkdir;
    }


    typedef SMBCFILE * (*smbc_open_fn) (SMBCCTX *c,
                                        const char *fname,
                                        int flags,
                                        mode_t mode);

    static inline smbc_open_fn smbc_getFunctionOpen (SMBCCTX *c)
    {
      return c->open;
    }


    typedef SMBCFILE * (*smbc_opendir_fn) (SMBCCTX *c, const char *fname);

    static inline smbc_opendir_fn smbc_getFunctionOpendir (SMBCCTX *c)
    {
      return c->opendir;
    }


    typedef ssize_t (*smbc_read_fn) (SMBCCTX *c,
                                     SMBCFILE *file,
                                     void *buf,
                                     size_t count);

    static inline smbc_read_fn smbc_getFunctionRead (SMBCCTX *c)
    {
      return c->read;
    }


    typedef struct smbc_dirent * (*smbc_readdir_fn) (SMBCCTX *c, SMBCFILE *dir);

    static inline smbc_readdir_fn smbc_getFunctionReaddir (SMBCCTX *c)
    {
      return c->readdir;
    }


    typedef int (*smbc_removexattr_fn) (SMBCCTX *context,
                                        const char *fname,
                                        const char *name);

    static inline smbc_removexattr_fn smbc_getFunctionRemovexattr (SMBCCTX *c)
    {
      return c->removexattr;
    }


    typedef int (*smbc_rename_fn) (SMBCCTX *ocontext,
                                   const char *oname,
                                   SMBCCTX *ncontext,
                                   const char *nname);

    static inline smbc_rename_fn smbc_getFunctionRename (SMBCCTX *c)
    {
      return c->rename;
    }


    typedef int (*smbc_rmdir_fn) (SMBCCTX *c, const char *fname);

    static inline smbc_rmdir_fn smbc_getFunctionRmdir (SMBCCTX *c)
    {
      return c->rmdir;
    }


    typedef int (*smbc_lseekdir_fn)(SMBCCTX *c,
                                    SMBCFILE *dir,
                                    off_t offset);

    static inline smbc_lseekdir_fn smbc_getFunctionLseekdir (SMBCCTX *c)
    {
      return c->lseekdir;
    }


    typedef int (*smbc_setxattr_fn) (SMBCCTX *context,
                                     const char *fname,
                                     const char *name,
                                     const void *value,
                                     size_t size,
                                     int flags);

    static inline smbc_setxattr_fn smbc_getFunctionSetxattr (SMBCCTX *c)
    {
      return c->setxattr;
    }


    typedef int (*smbc_stat_fn) (SMBCCTX *c, const char *fname, struct stat *s);

    static inline smbc_stat_fn smbc_getFunctionStat (SMBCCTX *c)
    {
      return c->stat;
    }


    typedef int (*smbc_unlink_fn) (SMBCCTX *c, const char *fname);

    static inline smbc_unlink_fn smbc_getFunctionUnlink (SMBCCTX *c)
    {
      return c->unlink;
    }


    typedef int (*smbc_utimes_fn) (SMBCCTX *c,
                                   const char *fname,
                                   struct timeval *tbuf);

    static inline smbc_utimes_fn smbc_getFunctionUtimes (SMBCCTX *c)
    {
      return c->utimes;
    }


    typedef ssize_t (*smbc_write_fn) (SMBCCTX *c,
                                      SMBCFILE *file,
                                      void *buf,
                                      size_t count);

    static inline smbc_write_fn smbc_getFunctionWrite (SMBCCTX *c)
    {
      return c->write;
    }


    typedef void (*smbc_get_auth_fn) (const char *srv,
                                      const char *shr,
                                      char *wg, int wglen,
                                      char *un, int unlen,
                                      char *pw, int pwlen);

    static inline void smbc_setFunctionAuthData (SMBCCTX *c,
                                                 smbc_get_auth_data_fn fn)
    {
      c->callbacks.auth_fn = fn;
    }


    static inline void smbc_setTimeout (SMBCCTX *c, int timeout)
    {
      c->timeout = timeout;
    }


    static inline void smbc_setUser (SMBCCTX *c, char *user)
    {
      c->user = user;
    }


    static inline void smbc_setWorkgroup (SMBCCTX *c, char *workgroup)
    {
      c->workgroup = workgroup;
    }

  #endif

#endif

