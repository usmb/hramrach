/* usmb - mount SMB shares via FUSE and Samba
 * Copyright (C) 2006-2009 Geoff Johnstone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include "password.h"
#include "utils.h"


bool password_read (char **out)
{
  struct termios attr, new;

  assert (NULL != out);
  *out = NULL;

  if (0 != tcgetattr (STDIN_FILENO, &attr))
  {
    perror ("tcgetattr");
    fputs ("Cannot configure terminal to read password securely.\n", stderr);
    return false;
  }

  new = attr;
  new.c_lflag &= ~ECHO;

  if (0 != tcsetattr (STDIN_FILENO, TCSAFLUSH, &new))
  {
    perror ("tcsetattr");
    fputs ("Cannot configure terminal to read password securely.\n", stderr);
    return false;
  }

  bool ok = false;
  char buff[1024];

  fputs ("\nPassword: ", stdout);
  fflush (stdout);
  ok = (buff == fgets (buff, sizeof (buff), stdin));
  fputc ('\n', stdout);

  if (0 != tcsetattr (STDIN_FILENO, TCSAFLUSH, &attr))
  {
    perror ("tcsetattr");
    fputs ("Failed to reset terminal.\n", stderr);
  }

  // strip a trailing '\n'.
  {
    size_t len = strlen (buff);

    if ((0 < len) && ('\n' == buff[len - 1]))
      buff[len - 1] = '\0';
  }

  *out = xstrdup (buff);
  memset (buff, 0, sizeof (buff));

  DEBUG (fprintf (stderr, "Password: %s\n", *out));

  return (NULL != *out);
}

