/* usmb - mount SMB shares via FUSE and Samba
 * Copyright (C) 2006-2009 Geoff Johnstone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VERSION_H
  #define VERSION_H

  #include <stdio.h>

  #define USMB_VERSION 0x20100212

  // a - alpha, b - beta, p - pre-release, s - stable
  #define USMB_VERSION_STATUS 'b'

  void show_about (FILE *fp);
  void show_version (FILE *fp);

#endif
