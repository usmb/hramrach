/* usmb - mount SMB shares via FUSE and Samba
 * Copyright (C) 2006-2009 Geoff Johnstone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef USMB_H
  #define USMB_H

  #include <stdbool.h>
  #include <stdint.h>
  #include <libsmbclient.h>

  extern SMBCCTX *ctx;

  char * make_url (const char *path) MUSTCHECK;

  bool create_smb_context (SMBCCTX **pctx) MUSTCHECK;
  void destroy_smb_context (SMBCCTX *ctx, int shutdown);

  /* fuse_file_info uses a uint64_t for a "File handle" */
  static inline uint64_t smbcfile_to_fd (SMBCFILE *file)
  {
    return (uint64_t)(uintptr_t)file;
  }


  static inline SMBCFILE * fd_to_smbcfile (uint64_t fd)
  {
    return (SMBCFILE *)(uintptr_t)fd;
  }

#endif
