/* usmb - mount SMB shares via FUSE and Samba
 * Copyright (C) 2006-2010 Geoff Johnstone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"
#include <sys/time.h>        // struct timeval needed by libsmbclient.h
#include <unistd.h>
#include <libsmbclient.h>
#include "samba3x-compat.h"
#include <fuse.h>
#include <assert.h>
#include <errno.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "conffile.h"
#include "options.h"
#include "password.h"
#include "usmb.h"
#include "usmb_dir.h"
#include "usmb_file.h"
#include "utils.h"
#include "version.h"


SMBCCTX *ctx;
static char *server, *share, *mountpoint, *options,
            *domain, *username, *password;


char * make_url (const char *path)
{
  assert (NULL != share);

  if ((NULL == path) || ('\0' == path[0]))
    return xstrdup (share);
  else
    return concat_strings (2, share, path);
}


static inline void do_strncpy (char *to, const char *from, int tolen)
{
  strncpy (to, from, tolen);
  to[tolen - 1] = '\0';
}


static void auth_fn (const char *srv UNUSED, const char *shr UNUSED,
                     char *wg, int wglen, char *un, int unlen,
                     char *pw, int pwlen)
{
  DEBUG (fprintf (stderr, "Authenticating for \\\\%s\\%s\n", srv, shr));
  DEBUG (fprintf (stderr, "Domain: %s; User: %s; Password: %s\n",
                  domain, username, password));

  if (NULL != domain)
    do_strncpy (wg, domain, wglen);

  do_strncpy (un, username, unlen);
  do_strncpy (pw, password, pwlen);
}


void destroy_smb_context (SMBCCTX *ctx_, int shutdown)
{
  // Samba frees the workgroup and user strings but we want to persist them.
  smbc_setWorkgroup (ctx_, NULL);
  smbc_setUser (ctx_, NULL);
  smbc_free_context (ctx_, shutdown);
}


bool create_smb_context (SMBCCTX **pctx)
{
  *pctx = smbc_new_context();

  if (NULL == *pctx)
  {
    perror ("Cannot create SMB context");
    return false;
  }

  smbc_setWorkgroup (*pctx, domain);
  smbc_setUser (*pctx, username);
  smbc_setTimeout (*pctx, 5000);
  smbc_setFunctionAuthData (*pctx, auth_fn);

  if (NULL == smbc_init_context (*pctx))
  {
    perror ("Cannot initialise SMB context");
    destroy_smb_context (*pctx, 1);
    return false;
  }

  return true;
}


static void * usmb_init (struct fuse_conn_info *conn UNUSED)
{
  DEBUG (fputs ("usmb_init()\n", stderr));
  return NULL;
}


static void usmb_destroy (void *unused UNUSED)
{
  DEBUG (fputs ("usmb_destroy()\n", stderr));
}


// probably won't (can't ?) implement these:
// readlink mknod symlink flush fsync

// no easy way of implementing these:
// access

#ifdef __lint
#define SET_ELEMENT(name,value) value
#else
#define SET_ELEMENT(name,value) name = value
#endif
static struct fuse_operations fuse_ops = {
  SET_ELEMENT (.getattr, usmb_getattr),
  SET_ELEMENT (.readlink, NULL),
  SET_ELEMENT (.getdir, NULL),
  SET_ELEMENT (.mknod, NULL),
  SET_ELEMENT (.mkdir, usmb_mkdir),
  SET_ELEMENT (.unlink, usmb_unlink),
  SET_ELEMENT (.rmdir, usmb_rmdir),
  SET_ELEMENT (.symlink, NULL),
  SET_ELEMENT (.rename, usmb_rename),
  SET_ELEMENT (.link, NULL),
  SET_ELEMENT (.chmod, usmb_chmod),
  SET_ELEMENT (.chown, NULL), // usmb_chown, --not implemented in libsmbclient
  SET_ELEMENT (.truncate, usmb_truncate),
  SET_ELEMENT (.utime, usmb_utime),
  SET_ELEMENT (.open, usmb_open),
  SET_ELEMENT (.read, usmb_read),
  SET_ELEMENT (.write, usmb_write),
  SET_ELEMENT (.statfs, usmb_statfs),
  SET_ELEMENT (.flush, NULL),
  SET_ELEMENT (.release, usmb_release),
  SET_ELEMENT (.fsync, NULL),
  SET_ELEMENT (.setxattr, usmb_setxattr),
  SET_ELEMENT (.getxattr, usmb_getxattr),
  SET_ELEMENT (.listxattr, usmb_listxattr),
  SET_ELEMENT (.removexattr, usmb_removexattr),
  SET_ELEMENT (.opendir, usmb_opendir),
  SET_ELEMENT (.readdir, usmb_readdir),
  SET_ELEMENT (.releasedir, usmb_releasedir),
  SET_ELEMENT (.fsyncdir, NULL),
  SET_ELEMENT (.init, usmb_init),
  SET_ELEMENT (.destroy, usmb_destroy),
  SET_ELEMENT (.access, NULL),
  SET_ELEMENT (.create, usmb_create),
  SET_ELEMENT (.ftruncate, usmb_ftruncate),
  SET_ELEMENT (.fgetattr, usmb_fgetattr),
  SET_ELEMENT (.lock, NULL),                   // TODO: implement
  SET_ELEMENT (.utimens, NULL),                // TODO: implement
  SET_ELEMENT (.bmap, NULL),                   // TODO: implement
};


static bool create_share_name (const char *server_, const char *sharename)
{
  size_t len = strlen ("smb:///") +
               strlen (server_) +
               strlen (sharename) + 1;

  if (NULL == (share = malloc (len)))
  {
    perror ("Cannot allocate share name");
    return false;
  }

  snprintf (share, len, "smb://%s/%s", server_, sharename);
  DEBUG (fprintf (stderr, "Share URL: %s\n", share));
  return true;
}


static void free_strings (char *sharename)
{
  xfree (sharename);
  clear_and_free (password);
  xfree (username);
  xfree (domain);
  xfree (options);
  xfree (mountpoint);
  xfree (share);
  xfree (server);
}


static bool check_credentials (void)
{
  char *url = make_url ("");
  if (NULL == url)
  {
    errno = ENOMEM;
    return false;
  }

  DEBUG (fprintf (stderr, "URL: %s\n", url));

  struct stat stat_;
  bool ret = (0 == (smbc_getFunctionStat (ctx) (ctx, url, &stat_)));

  free_errno (url);
  return ret;
}


static bool get_context (void)
{
  ctx = NULL;

  unsigned attempts = 3;

  while (0 != attempts--)
  {
    if ((NULL == password) && !password_read (&password))
      break;

    if (!create_smb_context (&ctx))
    {
      clear_and_free (password);
      password = NULL;
      ctx = NULL;
      break;
    }

    if (check_credentials())
      break;

    perror ("Connection failed");
    clear_and_free (password);
    password = NULL;

    destroy_smb_context (ctx, 1);
    ctx = NULL;
  }

  return (NULL != ctx);
}


int main (int argc, char **argv)
{
  const char *conffile, *mountid;
  bool umount;
  char *sharename = NULL;
  int ret = EXIT_FAILURE;

  if (sizeof (uint64_t) < sizeof (uintptr_t))
  {
    fputs ("usmb is not supported on this platform.\n", stderr);
    return EXIT_FAILURE;
  }

  {
    static char conf[256];
    snprintf (conf, sizeof (conf), "%s/.usmb.conf", getenv ("HOME"));
    conffile = conf;
  }

  if (!parse_args (&argc, &argv, &mountid, &conffile, &umount))
    return EXIT_FAILURE;

  if (!umount)
    show_about (stdout);

  if (!conffile_get_mount (conffile, mountid,
                           &server, &sharename, &mountpoint, &options,
                           &domain, &username, &password))
    return EXIT_FAILURE;

  if (umount)
  {
    execlp ("fusermount", "fusermount", "-u", mountpoint, NULL);
    perror ("Failed to execute fusermount");
  }

  else if (create_share_name (server, sharename) && get_context())
  {
    DEBUG (fprintf (stderr, "Username: %s\\%s\n", domain, username));

    int fuse_argc;
    char **fuse_argv;
    build_fuse_args (options, mountpoint, &fuse_argc, &fuse_argv);
    ret = fuse_main (fuse_argc, fuse_argv, &fuse_ops, NULL);
    destroy_smb_context (ctx, 1);
  }

  free_strings (sharename);
  return ret;
}

