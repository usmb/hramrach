/* usmb - mount SMB shares via FUSE and Samba
 * Copyright (C) 2006-2010 Geoff Johnstone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"
#include <assert.h>
#include <glib.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "options.h"
#include "utils.h"
#include "version.h"


static gboolean version = FALSE;
static gchar *conffile = NULL;
static gboolean debug = FALSE;
static gboolean nofork = FALSE;
static gboolean umount = FALSE;
static gchar **remaining = NULL;


static GOptionEntry entries[] = {
  { .long_name = "version",
    .short_name = 'v',
    .flags = 0,
    .arg = G_OPTION_ARG_NONE,
    .arg_data = &version,
    .description = "Show usmb, FUSE and Samba versions" },

  { .long_name = "version",
    .short_name = 'V',
    .flags = G_OPTION_FLAG_HIDDEN,
    .arg = G_OPTION_ARG_NONE,
    .arg_data = &version,
    .description = NULL },

  { .long_name = "config",
    .short_name = 'c',
    .flags = 0,
    .arg = G_OPTION_ARG_FILENAME,
    .arg_data = &conffile,
    .description = "usmb configuration file" },

  { .long_name = "debug",
    .short_name = 'd',
    .arg = G_OPTION_ARG_NONE,
    .arg_data = &debug,
    .description = "Debug mode" },

  { .long_name = "nofork",
    .short_name = 'f',
    .arg = G_OPTION_ARG_NONE,
    .arg_data = &nofork,
    .description = "Foreground operation" },

  { .long_name = "unmount",
    .short_name = 'u',
    .arg = G_OPTION_ARG_NONE,
    .arg_data = &umount,
    .description = "Unmount the given filesystem" },

  { .long_name = G_OPTION_REMAINING,
    .short_name = 0,
    .arg = G_OPTION_ARG_STRING_ARRAY,
    .arg_data = &remaining,
    .description = NULL },

  { .long_name = NULL }
};


bool parse_args (int *argc, char ***argv,
                 const char **mountid, const char **out_conffile,
                 bool *out_umount)
{
  GError *error = NULL;

  GOptionContext *context =
    g_option_context_new ("- mount SMB shares via FUSE and Samba");
  g_option_context_add_main_entries (context, entries, NULL);
  bool ret = g_option_context_parse (context, argc, argv, &error);
  g_option_context_free (context);

  if (false == ret)
  {
    fprintf (stderr, "Try `%s --help' for more information\n", (*argv)[0]);
    return false;
  }

  if (version)
  {
    show_version (stdout);
    exit (EXIT_SUCCESS);
  }

  if ((NULL == remaining) || (NULL == remaining[0]))
  {
    fputs ("No share ID given.\n", stderr);
    return false;
  }

  if (NULL != remaining[1])
  {
    fputs ("Too many arguments.\n", stderr);
    return false;
  }

  *out_umount = umount;
  *mountid = remaining[0];
  g_free (remaining);
  DEBUG (fprintf (stderr, "Mount ID: %s\n", *mountid));

  if (NULL != conffile)
    *out_conffile = conffile;

  return ret;
}


/* FUSE args are:
 *
 * argv[0]
 * -s
 * -d          -- if debug mode requested
 * -f          -- if foreground mode requested
 * -o ...      -- if any mount options in the config file
 * mount point
 */
#define MAXARGS 12
void build_fuse_args (const char *options, const char *mountpoint,
                      int *out_argc, char ***out_argv)
{
  static char USMB[] = "usmb";
  static char MINUS_S[] = "-s";
  static char MINUS_D[] = "-d";
  static char MINUS_F[] = "-f";
  static char MINUS_O[] = "-o";
  static char MAX_READ[] = "max_read=32768";

  assert (NULL != mountpoint);
  static char *argv[MAXARGS];

  int argc = 0;

  argv[argc++] = USMB;
  argv[argc++] = MINUS_S;

  if (debug)
    argv[argc++] = MINUS_D;

  // force -f in debug mode
#ifndef DEBUGON
  if (nofork)
#endif
    argv[argc++] = MINUS_F;

  argv[argc++] = MINUS_O;
  argv[argc++] = MAX_READ;

  if ((NULL != options) && ('\0' != options[0]))
  {
    argv[argc++] = MINUS_O;
    argv[argc++] = (char *)options;
  }

  argv[argc++] = (char *)mountpoint;
  argv[argc] = NULL;          // for good measure...

  assert (argc < MAXARGS);
  *out_argc = argc;
  *out_argv = argv;

  #ifdef DEBUGON
    for (int i = 0; i < argc; ++i)
      fprintf (stderr, "%d: %s\n", i, argv[i]);
  #endif
}

