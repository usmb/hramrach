/* usmb - mount SMB shares via FUSE and Samba
 * Copyright (C) 2006-2009 Geoff Johnstone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONFFILE_H
  #define CONFFILE_H

  #include <stdbool.h>

  bool conffile_get_mount (const char *filename, const char *key,
                           char **server, char **share,
                           char **mountpoint, char **options,
                           char **domain, char **username,
                           char **password) MUSTCHECK;
#endif
