/* usmb - mount SMB shares via FUSE and Samba
 * Copyright (C) 2006-2010 Geoff Johnstone
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"
#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "utils.h"
#include "xml.h"
#include "conffile.h"
#include "config.rng.h"


static bool check_conf_perms (const char *conffile, int fd)
{
  struct stat buf;

  if (0 == fstat (fd, &buf))
  {
    if (getuid() != buf.st_uid)
    {
      fprintf (stderr, "You do not own the configuration file %s.\n", conffile);
      return false;
    }

    if (buf.st_mode & (S_IRWXG | S_IRWXO))
    {
      fprintf (stderr, "Configuration file %s is accessible to non-owner.\n",
               conffile);
      return false;
    }
  }

  else
  {
    fprintf (stderr, "Cannot stat configuration file %s: %s.\n",
             conffile, strerror (errno));
    return false;
  }

  return true;
}


static bool conffile_read (const char *filename,
                           xmlDocPtr *doc,
                           xmlXPathContextPtr *ctx)
{
  int fd = open (filename, O_RDONLY);
  if (-1 == fd)
  {
    fprintf (stderr, "Cannot open %s: %s.\n", filename, strerror (errno));
    return false;
  }

  if (!check_conf_perms (filename, fd))
  {
    (void)close (fd);
    return false;
  }

  *doc = xmlReadFd (fd, NULL, NULL, XML_PARSE_NONET);
  (void)close (fd);

  if (NULL == *doc)
  {
    fprintf (stderr, "Cannot parse %s.\n", filename);
    return false;
  }

  if (!xml_validate_relaxng (*doc, rng_config))
  {
    fprintf (stderr, "%s isn't a valid USMB configuration.\n", filename);
    xmlFreeDoc (*doc);
    return false;
  }

  *ctx = xmlXPathNewContext (*doc);
  if (NULL == *ctx)
  {
    fputs ("Cannot create XPath context.\n", stderr);
    xmlFreeDoc (*doc);
    return false;
  }

  return true;
}


static bool do_xpath_text (xmlXPathContextPtr ctx,
                           const char *parent, const char *id,
                           const char *child, char **out)
{
  char xpath[2048];
  snprintf (xpath, sizeof (xpath),
            "/usmbconfig/%s[@id='%s']/%s/text()", parent, id, child);
  return xml_xpath_text (ctx, xpath, (void *)out);
}


bool conffile_get_mount (const char *filename, const char *key,
                         char **server, char **share,
                         char **mountpoint, char **options,
                         char **domain, char **username,
                         char **password)
{
  xmlDocPtr doc;
  xmlXPathContextPtr ctx;
  char xp[2048];
  char *creds = NULL;

  *server = *share = *mountpoint = *options = NULL;
  *domain = *username = *password = NULL;

  if (strchr (key, '\''))
  {
    fprintf (stderr, "Invalid share name: %s.\n", key);
    return false;
  }

  if (!conffile_read (filename, &doc, &ctx))
    return false;

  do {
    if (!do_xpath_text (ctx, "mount", key, "server", server)) break;
    if (!do_xpath_text (ctx, "mount", key, "share", share)) break;
    if (!do_xpath_text (ctx, "mount", key, "mountpoint", mountpoint)) break;
    (void)do_xpath_text (ctx, "mount", key, "options", options);

    snprintf (xp, sizeof (xp), "/usmbconfig/mount[@id='%s']", key);
    if (!xml_xpath_attr_value (ctx, xp, "credentials", &creds)) break;

    (void)do_xpath_text (ctx, "credentials", creds, "domain", domain);
    if (!do_xpath_text (ctx, "credentials", creds, "username", username)) break;

    if (!do_xpath_text (ctx, "credentials", creds, "password", password))
      *password = NULL;

    xmlXPathFreeContext (ctx);
    xmlFreeDoc (doc);

    return true;
    /*NOTREACHED*/
  } while (false /*CONSTCOND*/);

  fputs ("Invalid configuration.\n", stderr);

  xfree (*username);
  clear_and_free (*password);
  xfree (*domain);
  xfree (creds);
  xfree (*options);
  xfree (*mountpoint);
  xfree (*share);
  xfree (*server);

  xmlXPathFreeContext (ctx);
  xmlFreeDoc (doc);

  return false;
}

